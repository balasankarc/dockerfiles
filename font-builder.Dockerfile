FROM debian:buster-slim

RUN apt-get update \
    && apt-get install -y curl \
    && curl -sSL https://github.com/HinTak/Font-Validator/releases/download/FontVal-2.1.4/FontVal-2.1.4-ubuntu-10.7-x64.tgz | tar xvz \
    && mv FontVal-2.1.4-ubuntu-10.7-x64/FontValidator /usr/local/bin/FontValidator \
    && rm -rf FontVal-2.1.4-ubuntu-10.7-x64 /var/lib/apt/lists/*
